/*
 * File: TemplateSubHSM.c
 * Author: J. Edward Carryer
 * Modified: Gabriel H Elkaim
 *
 * Template file to set up a Heirarchical State Machine to work with the Events and
 * Services Framework (ES_Framework) on the Uno32 for the CMPE-118/L class. Note that
 * this file will need to be modified to fit your exact needs, and most of the names
 * will have to be changed to match your code.
 *
 * There is for a substate machine. Make sure it has a unique name
 *
 * This is provided as an example and a good place to start.
 *
 * History
 * When           Who     What/Why
 * -------------- ---     --------
 * 09/13/13 15:17 ghe      added tattletail functionality and recursive calls
 * 01/15/12 11:12 jec      revisions for Gen2 framework
 * 11/07/11 11:26 jec      made the queue static
 * 10/30/11 17:59 jec      fixed references to CurrentEvent in RunTemplateSM()
 * 10/23/11 18:20 jec      began conversion from SMTemplate.c (02/20/07 rev)
 */


/*******************************************************************************
 * MODULE #INCLUDE                                                             *
 ******************************************************************************/

#include "ES_Configure.h"
#include "ES_Framework.h"

#include "RoachFSM.h"
#include "BOARD.h"
//Uncomment these for the Roaches
#include "roach.h"
//#include "RoachFrameworkEvents.h"
#include <stdio.h>
#include "EventChecker.h"
#include "EventService.h"
#include "LightSensorEventService.h"
#include "MotorHelper.h" // use helper functions for roach motors
#include "ES_Timers.h"
#include "EventChecker.h"


/*******************************************************************************
 * MODULE #DEFINES                                                             *
 ******************************************************************************/

#define BumperTimer 0
#define ReverseTimer 1
#define EvadeForwardTimer 2
#define JigTimer 3

#define RearRight 0
#define RearLeft 1
#define FrontLeft  2
#define FrontRight 3
#define NoHit 5 

typedef enum {
    InitPSubState,
    Hiding,
    RunAway,
} RoachSubHSMState_t;

static const char *StateNames[] = {
	"InitPSubState",
	"Hiding",
	"RunAway",
};



/*******************************************************************************
 * PRIVATE FUNCTION PROTOTYPES                                                 *
 ******************************************************************************/
/* Prototypes for private functions for this machine. They should be functions
   relevant to the behavior of this state machine */

/*******************************************************************************
 * PRIVATE MODULE VARIABLES                                                            *
 ******************************************************************************/
/* You will need MyPriority and the state variable; you may need others as well.
 * The type of state variable should match that of enum in header file. */


static uint8_t MyPriority;

static RoachSubHSMState_t CurrentState = InitPSubState; // <- change name to match ENUM

/*******************************************************************************
 * PUBLIC FUNCTIONS                                                            *
 ******************************************************************************/

/**
 * @Function InitTemplateSubHSM(uint8_t Priority)
 * @param Priority - internal variable to track which event queue to use
 * @return TRUE or FALSE
 * @brief This will get called by the framework at the beginning of the code
 *        execution. It will post an ES_INIT event to the appropriate event
 *        queue, which will be handled inside RunTemplateFSM function. Remember
 *        to rename this to something appropriate.
 *        Returns TRUE if successful, FALSE otherwise
 * @author J. Edward Carryer, 2011.10.23 19:25 */
uint8_t InitDarkRoachSubHSM(void) {
    ES_Event returnEvent;

    CurrentState = InitPSubState;
    ES_Timer_InitTimer(BumperTimer, 5);
    //returnEvent = DarkRoachSubHSM(INIT_EVENT);
    // Init Sub State Machines
    if (returnEvent.EventType == ES_NO_EVENT) {
        return TRUE;
    }
    return FALSE;
}

/**
 * @Function RunTemplateSubHSM(ES_Event ThisEvent)
 * @param ThisEvent - the event (type and param) to be responded.
 * @return Event - return event (type and param), in general should be ES_NO_EVENT
 * @brief This function is where you implement the whole of the heirarchical state
 *        machine, as this is called any time a new event is passed to the event
 *        queue. This function will be called recursively to implement the correct
 *        order for a state transition to be: exit current state -> enter next state
 *        using the ES_EXIT and ES_ENTRY events.
 * @note Remember to rename to something appropriate.
 *       The lower level state machines are run first, to see if the event is dealt
 *       with there rather than at the current level. ES_EXIT and ES_ENTRY events are
 *       not consumed as these need to pass pack to the higher level state machine.
 * @author J. Edward Carryer, 2011.10.23 19:25
 * @author Gabriel H Elkaim, 2011.10.23 19:25 */
static int HitFlag = 5;
uint8_t DarkMakeTransition = FALSE; // use to flag transition
RoachSubHSMState_t nextState; // <- change type to correct enum
static RoachSubHSMState_t DarkLastState;

ES_Event DarkRoachSubHSM(ES_Event ThisEvent) {


    //    ES_Tattle(); // trace call stack

    switch (ThisEvent.EventType) {
        case ES_TIMEOUT:
            if (ThisEvent.EventParam == BumperTimer) {
                ES_Timer_InitTimer(BumperTimer, 5);
                CheckBumpers();
            } else if (ThisEvent.EventParam == ReverseTimer) {
                nextState = Hiding;
            }
            break;
        case ES_NO_EVENT:
            //            printf("IN NO EVENT");
            break;
        case ES_TIMERACTIVE:
            break;

    }

    switch (CurrentState) {
        case InitPSubState: // If current state is initial Psedudo State
            if (ThisEvent.EventType == ES_INIT)// only respond to ES_Init
            {
                // this is where you would put any actions associated with the
                // transition from the initial pseudo-state into the actual
                // initial state

                // now put the machine into the actual initial state
                nextState = Hiding;
                DarkMakeTransition = TRUE;
                ThisEvent.EventType = ES_NO_EVENT;
            }
            break;

        case Hiding: // in the first state, replace this with correct names
            MotorsStop();
            //             printf("In Hiding\n");
            //            if (ThisEvent.EventType == LIGHT_TRANS) {
            //                return ThisEvent;
            //            }
            if (ThisEvent.EventType == FR_BUMP_HIT) {
                HitFlag = FrontRight;
                //PostRoachHSM(ThisEvent);
                //                printf("HIde FR ");
                nextState = RunAway;
                ES_Timer_InitTimer(ReverseTimer, 2000); // 5 sec reverse
            } else if (ThisEvent.EventType == FL_BUMP_HIT) {
                HitFlag = FrontLeft;
                //                PostRoachHSM(ThisEvent);
                //                printf("HIde FL ");
                nextState = RunAway;
                ES_Timer_InitTimer(ReverseTimer, 2000); // 5 sec reverse
            } else if (ThisEvent.EventType == RR_BUMP_HIT) {
                //                printf("HIde RR ");
                //                PostRoachHSM(ThisEvent);
                HitFlag = RearRight;
                nextState = RunAway;
                ES_Timer_InitTimer(ReverseTimer, 2000); // 5 sec reverse
            } else if (ThisEvent.EventType == RL_BUMP_HIT) {
                HitFlag = RearLeft;
                //                PostRoachHSM(ThisEvent);
                //                printf("HIde RL ");
                nextState = RunAway;
                ES_Timer_InitTimer(ReverseTimer, 2000); // 5 sec reverse
            } else {
                HitFlag = NoHit;
            }
            break;
        case RunAway:
            //            printf("Run to Hide ");
            //            if (ThisEvent.EventType == LIGHT_TRANS) {
            //                return ThisEvent;
            //            }

            if ((ThisEvent.EventType == ES_TIMEOUT) && (ThisEvent.EventParam == ReverseTimer)) {
                nextState = Hiding;
                DarkMakeTransition = TRUE;
            }
            //            printf("HitFlag:%i  ", HitFlag);
            if (HitFlag == FrontRight) {
                //                printf("Shouldnt be");
                //                printf("FR");
                MotorsReverseLeft(100);
            } else if (HitFlag == FrontLeft) {
                //                printf("FL");
                MotorsReverseRight(100);
            } else if (HitFlag == RearRight) {
                //                printf("RR");
                MotorsForwardLeft(100);
            } else if (HitFlag == RearLeft) {
                //                printf("RL");
                MotorsForwardRight(100);
            }
            HitFlag = NoHit;

            break;


        default: // all unhandled states fall into here
            break;
    } // end switch on Current State
    if (CurrentState != DarkLastState) {
        DarkLastState = CurrentState;
        //        printf("Gonna Post  ");
        PostRoachHSM(ThisEvent);
    } //event detected

    if (DarkMakeTransition == TRUE) { // making a state transition, send EXIT and ENTRY
        // recursively call the current state with an exit event
        //        RunRoachHSM(EXIT_EVENT); // <- rename to your own Run function
        //        printf("Transition    ");

        CurrentState = nextState;
        //        RunRoachHSM(ENTRY_EVENT); // <- rename to your own Run function
    }
    ES_Tail(); // trace call stack end
    ThisEvent.EventType = ES_NO_EVENT; // Fixed Error?
    return ThisEvent;
}


/*******************************************************************************
 * PRIVATE FUNCTIONS                                                           *
 ******************************************************************************/

