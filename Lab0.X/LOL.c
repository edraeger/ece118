#include "BOARD.h"
#include "BOARD.h"
#include "serial.h"
#include "roach.h"

#include <stdio.h>
#include <math.h>
#include <xc.h>
#include <sys/attribs.h>
#include <GenericTypeDefs.h>

#include "pwm.h"
#include "serial.h"
#include "AD.h"

#include <stdio.h>

unsigned int wait;

#define LIGHT_THRESHOLD 470
#define DARK_THRESHOLD 530

#define FLEFT_BUMP_MASK (1)
#define FRIGHT_BUMP_MASK (1<<1)
#define RLEFT_BUMP_MASK (1<<2)
#define RRIGHT_BUMP_MASK (1<<3)

#define DELAY(x)    for (wait = 0; wait <= x; wait++) {asm("nop");}
#define A_BIT       18300
#define A_BIT_MORE  36600
#define YET_A_BIT_LONGER (A_BIT_MORE<<2)
#define A_LOT       183000
#define NUM_TIMES_REPEAT_LED 5
#define MOTOR_TIME (A_LOT<<2)

#define LOW_BAT 263
#define HIGH_BAT 310

uint16_t LastLight = 0;

//unsigned char IfDark(void) {
//    uint16_t Dark = FALSE;
//    Dark = if ((Roach_LightLevel() > DARK_THRESHOLD) && (Roach_LightLevel() < LIGHT_THRESHOLD));
//    if (Dark) {
//        LastLight = Roach_LightLevel();
//    }
//    return Dark;
//}
//
//unsigned char IfLight(void) {
//    uint16_t Light = FALSE;
//    Light = if ((Roach_LightLevel() < DARK_THRESHOLD) && (Roach_LightLevel() > LIGHT_THRESHOLD));
//    if (Light) {
//        LastLight = Roach_LightLevel();
//    }
//    return Light;
//}

void FlashBar(uint8_t Num) {
    unsigned int i;
    unsigned int x;
    Roach_LEDSSet(0);
    for (i = 0; i < Num; i++) {
        Roach_LEDSSet(0xFFF);
        DELAY(YET_A_BIT_LONGER);
        Roach_LEDSSet(0x000);
        DELAY(YET_A_BIT_LONGER);
    }
}

typedef enum {
    DARK,
    LIGHT
} LastLightState;

static LastLightState CurrentLight;



/**
 * @Function Roach_ReadBumpers(void)
 * @param None.
 * @return 4-bit value representing all four bumpers in following order: front left,front right, rear left, rear right
 * @brief  Returns the state of all 4 bumpers
 * @author Max Dunne, 2012.01.06 */
//unsigned char Roach_ReadBumpers(void)

void Test(void) {
    // FrontLeft
    unsigned int Batt;
    unsigned int LightLvl;
    unsigned int i, j;

    switch (Roach_ReadBumpers()) {
        case FLEFT_BUMP_MASK:
            FlashBar(1);
            Batt = Roach_BatteryVoltage();
            printf("Battery Voltage is:%u\n", Batt);
            DELAY(A_LOT);
            printf("DONE FOO\n");
            DELAY(A_BIT);
            printf("Battery Test Complete!\n");
            FlashBar(1);
            break;

        case FRIGHT_BUMP_MASK:
            FlashBar(2);
            DELAY(A_LOT);
            for (i = 0; i < A_LOT >> 2; i++) {
                LightLvl = Roach_LightLevel();
                if (i % 10000 == 0) {
                    printf("Current Light Level: %d\n", LightLvl);
                }

                LightLvl *= 12;
                LightLvl /= 1023;
                Roach_BarGraph(LightLvl);
            }
            printf("Light Level Test Complete!\n");
            DELAY(A_LOT);
            FlashBar(2);
            break;
                
        case RLEFT_BUMP_MASK:
            FlashBar(3);
            printf("LED TEST\n");
            
            
            printf("LED TEST Complete!\n");
            //            break;
            //            case RRIGHT_BUMP_MASK:
            //            break;
    }
}

int main(void) {
    BOARD_Init();
    Roach_Init();
    unsigned int Currentbat;
    printf("To test a roach, click a bumper. Each Bumper runs a specific test\n");
    printf("Front Left: Display the current Battery Voltage\n");
    printf("Front Right: Display the light level live\n");
    printf("Rear Left: Test Left Motor\n");
    printf("Rear Right: Test Right Motor\n");
    while (1) {
        Test();
    }
}



//       // NOP();
//            // To move the Roach forward, we set both of the Roach's motors to the same speed
//            Roach_LeftMtrSpeed(50);
//            Roach_RightMtrSpeed(50);
//            // NOP();
//            // Return Back to starting position Backwards 
//            Roach_LeftMtrSpeed(-50);
//            Roach_RightMtrSpeed(-50);
//            //  NOP();
//            // Stop the Roach
//            Roach_LeftMtrSpeed(0);
//            Roach_RightMtrSpeed(0);
//            //  NOP();