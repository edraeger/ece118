/* 
 * File:   MotorHelper.h
 * Author: edraeger
 *
 * Created on April 10, 2023, 1:47 PM
 */
#include "roach.h"
#include "BOARD.h"


/**
 * @param NONE
 * @return TRUE or FALSE
 * @brief Stops the Roach by setting Motor speeds to zero
 *        Returns TRUE if successful, FALSE otherwise
 * @author E Draeger, 20231.4.10*/
char MotorsStop(void);


/**
 * @param NONE
 * @return TRUE or FALSE
 * @brief Moves the Roach forward by setting Motor speeds to "speed"
 *        Returns TRUE if successful, FALSE otherwise
 * @author E Draeger, 20231.4.10*/
char MotorsMoveForward(int speed);


/**
 * @param speed: how fast you want the motor to do whatever
 * @return TRUE or FALSE
 * @brief Moves roach by setting RIGHT motor to "speed" and LEFT motor to half
 *        Returns TRUE if successful, FALSE otherwise
 * @author E Draeger, 20231.4.10*/
char MotorsForwardLeft(int speed);


/**
 * @param speed: how fast you want the motor to do whatever
 * @return TRUE or FALSE
 * @brief Moves roach by setting LEFT motor to "speed" and RIGHT motor to half
 *        Returns TRUE if successful, FALSE otherwise
 * @author E Draeger, 20231.4.10*/
char MotorsForwardRight(int speed);

/**
 * @param speed: how fast you want the motor to do whatever
 * @return TRUE or FALSE
 * @brief Moves Roach BACK and to LEFT(so front points RIGHT) by setting RIGHT 
 *        motor to "-speed" and LEFT motor to half
 *        Returns TRUE if successful, FALSE otherwise
 * @author E Draeger, 20231.4.10*/
char MotorsReverseLeft(int speed);


/**
 * @param speed: how fast you want the motor to do whatever
 * @return TRUE or FALSE
 * @brief Moves Roach BACK and to RIGHT(so front points LEFT) by setting LEFT
 *        motor to "-speed" and RIGHT motor to half
 *        Returns TRUE if successful, FALSE otherwise
 * @author E Draeger, 20231.4.10*/
char MotorsReverseRight(int speed);