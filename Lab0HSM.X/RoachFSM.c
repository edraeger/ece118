/*
 * File: TemplateFSM.c
 * Author: J. Edward Carryer
 * Modified: Gabriel H Elkaim
 *
 * Template file to set up a Flat State Machine to work with the Events and Services
 * Frameword (ES_Framework) on the Uno32 for the CMPE-118/L class. Note that this file
 * will need to be modified to fit your exact needs, and most of the names will have
 * to be changed to match your code.
 *
 * This is provided as an example and a good place to start.
 *
 *Generally you will just be modifying the statenames and the run function
 *However make sure you do a find and replace to convert every instance of
 *  "Template" to your current state machine's name
 * History
 * When           Who     What/Why
 * -------------- ---     --------
 * 09/13/13 15:17 ghe      added tattletail functionality and recursive calls
 * 01/15/12 11:12 jec      revisions for Gen2 framework
 * 11/07/11 11:26 jec      made the queue static
 * 10/30/11 17:59 jec      fixed references to CurrentEvent in RunTemplateSM()
 * 10/23/11 18:20 jec      began conversion from SMTemplate.c (02/20/07 rev)
 */


/*******************************************************************************
 * MODULE #INCLUDE                                                             *
 ******************************************************************************/

#include "ES_Configure.h"
#include "ES_Framework.h"

#include "RoachFSM.h"
#include "BOARD.h"
//Uncomment these for the Roaches
#include "roach.h"
//#include "RoachFrameworkEvents.h"
#include <stdio.h>
#include "EventChecker.h"
#include "EventService.h"
#include "LightSensorEventService.h"
#include "MotorHelper.h" // use helper functions for roach motors
#include "ES_Timers.h"


/*******************************************************************************
 * MODULE #DEFINES                                                             *
 ******************************************************************************/
#define DARK_THRESHOLD 550
#define LIGHT_THRESHOLD 510

// Timer Defines
#define BumperTimer 0
#define ReverseTimer 1
#define EvadeForwardTimer 2


/*******************************************************************************
 * PRIVATE FUNCTION PROTOTYPES                                                 *
 ******************************************************************************/
/* Prototypes for private functions for this machine. They should be functions
   relevant to the behavior of this state machine.*/


/*******************************************************************************
 * PRIVATE MODULE VARIABLES                                                            *
 ******************************************************************************/

/* You will need MyPriority and the state variable; you may need others as well.
 * The type of state variable should match that of enum in header file. */

typedef enum {
    InitPState,
    Hiding,
    Forward,
    Backward,
    EvadeForward,
} FSMState_t;

static const char *StateNames[] = {
	"InitPState",
	"Hiding",
	"Forward",
	"Backward",
	"EvadeForward",
};


static FSMState_t CurrentState = InitPState; // <- change enum name to match ENUM
static uint8_t MyPriority;


/*******************************************************************************
 * PUBLIC FUNCTIONS                                                            *
 ******************************************************************************/

/**
 * @Function InitTemplateFSM(uint8_t Priority)
 * @param Priority - internal variable to track which event queue to use
 * @return TRUE or FALSE
 * @brief This will get called by the framework at the beginning of the code
 *        execution. It will post an ES_INIT event to the appropriate event
 *        queue, which will be handled inside RunTemplateFSM function. Remember
 *        to rename this to something appropriate.
 *        Returns TRUE if successful, FALSE otherwise
 * @author J. Edward Carryer, 2011.10.23 19:25 */
uint8_t InitRoachFSM(uint8_t Priority) {
    MyPriority = Priority;
    // put us into the Initial PseudoState
    CurrentState = InitPState;
    ES_Timer_InitTimer(BumperTimer, 5);
    // post the initial transition event
    if (ES_PostToService(MyPriority, INIT_EVENT) == TRUE) {
        return TRUE;
    } else {
        return FALSE;
    }
}

/**
 * @Function PostTemplateFSM(ES_Event ThisEvent)
 * @param ThisEvent - the event (type and param) to be posted to queue
 * @return TRUE or FALSE
 * @brief This function is a wrapper to the queue posting function, and its name
 *        will be used inside ES_Configure to point to which queue events should
 *        be posted to. Remember to rename to something appropriate.
 *        Returns TRUE if successful, FALSE otherwise
 * @author J. Edward Carryer, 2011.10.23 19:25 */
uint8_t PostRoachFSM(ES_Event ThisEvent) {
    return ES_PostToService(MyPriority, ThisEvent);
}

/**
 * @Function RunTemplateFSM(ES_Event ThisEvent)
 * @param ThisEvent - the event (type and param) to be responded.
 * @return Event - return event (type and param), in general should be ES_NO_EVENT
 * @brief This function is where you implement the whole of the flat state machine,
 *        as this is called any time a new event is passed to the event queue. This
 *        function will be called recursively to implement the correct order for a
 *        state transition to be: exit current state -> enter next state using the
 *        ES_EXIT and ES_ENTRY events.
 * @note Remember to rename to something appropriate.
 *       Returns ES_NO_EVENT if the event have been "consumed."
 * @author J. Edward Carryer, 2011.10.23 19:25 */
ES_Event MyEvent;
static FSMState_t nextState; // <- need to change enum type here
uint8_t makeTransition = FALSE; // use to flag transition
static FSMState_t LastState;
int roachSpeed;
int LorR;

ES_Event RunRoachFSM(ES_Event ThisEvent) {
    ES_Event ReturnEvent;
    
//        ES_Tattle(); // trace call stack
    //CheckLightLevel();
    //    printf("Entered Run Roach");
//    printf("%d", ThisEvent.EventType);


    //Event Deal with-er
    switch (ThisEvent.EventType) {
            // Timer Expiry Event Deal with-er
        case ES_TIMEOUT:
            if (ThisEvent.EventParam == BumperTimer) {
                ES_Timer_InitTimer(BumperTimer, 5); // 5ms

                CheckBumpers();
                //                printf("hewo timeout");
            }

            //            if (ThisEvent.EventParam == ReverseTimer) {
            //                nextState = Forward;
            //            } else if (ThisEvent.EventParam == EvadeForwardTimer) {
            //                nextState = Backward;
            //            } 
            break;
        case ES_NO_EVENT:
            //            printf("IN NO EVENT");
            break;
        case ES_TIMERACTIVE:
            break;
    }
    switch (CurrentState) {
        case InitPState: // If current state is initial Psedudo State

            if (ThisEvent.EventType == ES_INIT)// only respond to ES_Init
            {
                //                printf("In INIT STATE \n");
                // this is where you would put any actions associated with the

                // transition from the initial pseudo-state into the actual
                // initial state
                //                CheckLightLevel();
                //                printf("Dark Trans: %u, Our: %u\n", DARK_TRANS, MyEvent);

            }

            nextState = Hiding;
            makeTransition = TRUE;
            ThisEvent.EventType = ES_NO_EVENT;
            break;

        case Hiding: // in the first state, replace this with appropriate state
            MotorsStop();
            if (ThisEvent.EventType == LIGHT_TRANS) {
                nextState = Forward;
            }
            break;

        case Forward:
            if (ThisEvent.EventType == DARK_TRANS) {
                nextState = Hiding;

            }
            //            printf("FSM: %d", ThisEvent.EventType);

            if (ThisEvent.EventType == (FR_BUMP_HIT)) {
                nextState = Backward;
                LorR = TRUE;
                ES_Timer_InitTimer(ReverseTimer, 5000); // 5 sec reverse

            } else if (ThisEvent.EventType == (FL_BUMP_HIT)) {
                nextState = Backward;
                LorR = FALSE;
                ES_Timer_InitTimer(ReverseTimer, 2000); // 5 sec reverse
            }
            MotorsMoveForward(80);
            break;

        case Backward:
            //            printf("Event Type & Param:%d, %d", ThisEvent.EventType, ThisEvent.EventParam);
            if (ThisEvent.EventType == DARK_TRANS) {
                nextState = Hiding;

            }
            
            if ((ThisEvent.EventType == ES_TIMEOUT) && (ThisEvent.EventParam == ReverseTimer)) {
                //              printf("Reverse Timed out");
                ES_Timer_InitTimer(ReverseTimer, 2000); // 5 sec reverse
                nextState = Forward;
            }

            if (LorR == TRUE) {
                MotorsReverseRight(60);
            } else if (LorR == FALSE) {
                MotorsReverseLeft(60);
            }
            if (ThisEvent.EventType == (RR_BUMP_HIT)) {
                nextState = Forward; // changed from evade forward
                LorR = TRUE;
                ES_Timer_InitTimer(EvadeForwardTimer, 500); // 5 sec forward
            } else if (ThisEvent.EventType == (RL_BUMP_HIT)) {
                nextState = Forward; // changed from evade forward
                LorR = FALSE;
                ES_Timer_InitTimer(EvadeForwardTimer, 500); // 5 sec forward
            }

            break;
        case EvadeForward:
//            printf("Event Type & Param:%d, %d", ThisEvent.EventType, ThisEvent.EventParam);
            if ((ThisEvent.EventType == ES_TIMEOUT)) {
                printf("Backward State Param: %u\n",ThisEvent.EventParam);
                nextState = Forward; // Forget this state, timeout param below doesn't work
            }
            
            if ((ThisEvent.EventType == ES_TIMEOUT) && (ThisEvent.EventParam == EvadeForwardTimer)) {
                printf("w9orks");
                nextState = Backward;
            }
            if (LorR == TRUE) { // True if Rear Right is hit
                ES_Timer_InitTimer(ReverseTimer, 50);
                MotorsForwardRight(80);
                //MotorsForwardLeft(60);
            } else if (LorR == FALSE) { // False if Rear Left is hit
                ES_Timer_InitTimer(ReverseTimer, 50);
                //MotorsForwardRight(60);
                MotorsForwardLeft(80);
            }
            
            if (ThisEvent.EventType == (FR_BUMP_HIT)) {
                nextState = Backward;
                LorR = TRUE;
                
            } else if (ThisEvent.EventType == (FL_BUMP_HIT)) {
                nextState = Backward;
                LorR = FALSE;
                
            }
            break;
        default: // all unhandled states fall into here
            //            printf("IN DEFAULT");
            break;
    } // end switch on Current State

    if (CurrentState != LastState) {
        LastState = CurrentState;
        PostRoachFSM(ThisEvent);
    } //event detected

    if (makeTransition == TRUE) { // making a state transition, send EXIT and ENTRY
        // recursively call the current state with an exit event
        //        RunRoachFSM(EXIT_EVENT);
        CurrentState = nextState;
        //        RunRoachFSM(ENTRY_EVENT);
    }
    ES_Tail(); // trace call stack end
    ReturnEvent.EventType = ES_NO_EVENT;
    return ReturnEvent;
}


/*******************************************************************************
 * PRIVATE FUNCTIONS                                                           *
 ******************************************************************************/
