#include "MotorHelper.h"
// Libraries Copied from roach.c just in case
#include "BOARD.h"
#include <xc.h>

#include "roach.h"
#include "pwm.h"
#include "serial.h"
#include "AD.h"

/**
 * @param NONE
 * @return TRUE or FALSE
 * @brief Stops the Roach by setting Motor speeds to zero
 *        Returns TRUE if successful, FALSE otherwise
 * @author E Draeger, 20231.4.10*/
char MotorsStop(void){
    int roachSpeed = 0;
    Roach_RightMtrSpeed(roachSpeed);
    Roach_LeftMtrSpeed(roachSpeed);
}
    

/**
 * @param NONE
 * @return TRUE or FALSE
 * @brief Moves the Roach forward by setting Motor speeds to "speed"
 *        Returns TRUE if successful, FALSE otherwise
 * @author E Draeger, 20231.4.10*/
char MotorsMoveForward(int speed){
    Roach_RightMtrSpeed(speed);
    Roach_LeftMtrSpeed(speed);
}


/**
 * @param speed: how fast you want the motor to do whatever
 * @return TRUE or FALSE
 * @brief Moves roach by setting RIGHT motor to "speed" and LEFT motor to half
 *        Returns TRUE if successful, FALSE otherwise
 * @author E Draeger, 20231.4.10*/
char MotorsForwardLeft(int speed){
    Roach_RightMtrSpeed(speed);
    Roach_LeftMtrSpeed(speed/2);
}
    

/**
 * @param speed: how fast you want the motor to do whatever
 * @return TRUE or FALSE
 * @brief Moves roach by setting LEFT motor to "speed" and RIGHT motor to half
 *        Returns TRUE if successful, FALSE otherwise
 * @author E Draeger, 20231.4.10*/
char MotorsForwardRight(int speed){
    Roach_RightMtrSpeed(speed/2);
    Roach_LeftMtrSpeed(speed);
}

/**
 * @param speed: how fast you want the motor to do whatever
 * @return TRUE or FALSE
 * @brief Moves Roach BACK and to LEFT(so front points LEFT) by setting RIGHT 
 *        motor to "-speed" and LEFT motor to half
 *        Returns TRUE if successful, FALSE otherwise
 * @author E Draeger, 20231.4.10*/
char MotorsReverseLeft(int speed){
    Roach_RightMtrSpeed(-speed);
    Roach_LeftMtrSpeed((-speed)/2);
}


/**
 * @param speed: how fast you want the motor to do whatever
 * @return TRUE or FALSE
 * @brief Moves Roach BACK and to RIGHT(so front points LEFT) by setting LEFT
 *        motor to "-speed" and RIGHT motor to half
 *        Returns TRUE if successful, FALSE otherwise
 * @author E Draeger, 20231.4.10*/
char MotorsReverseRight(int speed){
    Roach_RightMtrSpeed((-speed)/2);
    Roach_LeftMtrSpeed(-speed);
}


